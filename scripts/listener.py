import threading


class Listener:

    def __init__(self, config, preProcessor, processor, stream):
        self._config = config
        self._stream = stream
        self._preProcessor = preProcessor
        self._processor = processor

    def start(self,):
        # start pre processor thread to validate incoming tweets
        threads_num = self._config['PRE_PROCESSOR_THREAD_COUNT']

        for i in range(threads_num):
            t = threading.Thread(name="PreProcessorThread-"+str(i),
                                 target=self._preProcessor.process, args=())
            t.start()

        # start processor thread to determine if tweets are positive/negative
        threads_num = self._config['PROCESSOR_THREAD_COUNT']
        for i in range(threads_num):
            t = threading.Thread(name="ProcessorThread-"+str(i),
                                 target=self._processor.process, args=())
            t.start()

        # get hashtags from config and append # to send to twitter
        tags = "#" + ',#'.join(map(str, self._config['HASHTAGS']))

        # start twitter real-time api listener
        self._stream.statuses.filter(
            track=tags, language="en")
