from twython import TwythonStreamer

# twython recommended usage of their package


class Streamer(TwythonStreamer):

    # initialize with custom queue
    def __init__(self, consumer_key, consumer_secret, token, token_secret, preProcessorQueue):
        self._que = preProcessorQueue
        super(Streamer, self).__init__(
            consumer_key, consumer_secret, token, token_secret)

    # when twitter sends us a tweet, add it to the pre-processor queue
    def on_success(self, data):
        self._que.put(data)

    # if error receiving tweet, print
    def on_error(self, status_code, data):
        print(status_code, data)
