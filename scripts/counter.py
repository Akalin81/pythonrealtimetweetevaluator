import threading

# thread safe counter that keeps count, tags and wordlist to check against


class Counter:

    def __init__(self, wordList, initial=0):
        self._value = initial
        self._tags = set()
        self._wordList = wordList
        self._lock = threading.Lock()

    def increment(self, tag, num=1):
        with self._lock:
            self._value += num
            self._tags.add(tag)

    def count(self,):
        return self._value

    def tags(self,):
        return self._tags

    def filter(self, words):
        # intersect operation runs at O(min(n, m)) where n is the count of first list, and m is the count of second list
        return self._wordList.intersection(words)
