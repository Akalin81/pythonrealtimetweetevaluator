import threading
import time
from datetime import datetime


class Processor:
    def __init__(self, processorQueue, mostPositive, mostNegative, logger):
        self._processorQueue = processorQueue
        self._mostPositive = mostPositive
        self._mostNegative = mostNegative
        self._logger = logger

    def process(self,):
        while(True):
            # get item from processor queue
            processorDto = self._processorQueue.get()
            # getthe words that exist in both positive and negative list
            positiveWords = self._mostPositive.filter(processorDto.data)
            negativeWords = self._mostNegative.filter(processorDto.data)

            # determine counts of each result
            posWordCount = len(positiveWords)
            negWordCount = len(negativeWords)

            # by default print result
            printCounts = True

            # determine if positive or negative is more and increment tag accordingly.
            if posWordCount == negWordCount:
                printCounts = False
            elif posWordCount > negWordCount:
                self._mostPositive.increment(processorDto.tag)
            elif negWordCount > posWordCount:
                self._mostNegative.increment(processorDto.tag)

            # print running total
            if printCounts:
                self._logger.printTotals()

            self._processorQueue.task_done()
