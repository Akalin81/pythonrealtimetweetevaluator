import threading
import time
import json
import re
import string

from .processorDto import ProcessorDto


class PreProcessor:
    def __init__(self, preProcessorQueue, processorQueue, ignoreList, config):
        self._preProcessorQueue = preProcessorQueue
        self._processorQueue = processorQueue
        self._ignoreList = ignoreList
        self._config = config

    def process(self,):
        while(True):
            # get item from the pre processor queue
            item = self._preProcessorQueue.get()

            # determine if tweet is extended
            extended = 'extended_tweet' in item

            # check for tag count and selected tag
            tags = item['entities']['hashtags']
            if extended:
                tags = item['extended_tweet']['entities']['hashtags']

            count = 0
            selectedTag = ""
            for tag in tags:
                if tag['text'].lower() in map(str.lower, self._config['HASHTAGS']):
                    count += 1
                    selectedTag = "#" + tag['text'].capitalize()

            # ignore if multiple tag or no tag (due to twitter api sending retweets of searched tags that do not contain the tags we are looking for)
            if count == 1:
                # get tweet content
                text = item['text']
                if extended:
                    text = item['extended_tweet']['full_text']

                # clean up urls
                text = re.sub(r'http\S+', '', text)

                # split cleaned up string into distinct word list and remove non ascii letters
                wordList = re.findall('[%s]+' %
                                      string.ascii_letters, text.lower())

                # remove items from ignore list (ex: I, You, We, etc.)
                wordList = [s for s in wordList if s not in self._ignoreList]

                # convert to set to get unique words and add it to DTO along with tag
                processorDto = ProcessorDto(selectedTag, set(wordList))

                # put the new DTO into processor queue to determine positive/negative ratio
                self._processorQueue.put(processorDto)

            self._preProcessorQueue.task_done()
