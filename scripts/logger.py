class Logger:

    def __init__(self, mostPositive, mostNegative):
        self._mostPositive = mostPositive
        self._mostNegative = mostNegative

    def printTotals(self,):
        positiveText = ""
        negativeText = ""
        if self._mostPositive.count() > 0:
            positiveText = "Most Positive: " + \
                ",".join(map(str, self._mostPositive.tags())) + \
                " (" + str(self._mostPositive.count()) + ") "

        if self._mostNegative.count() > 0:
            negativeText = "Most Negative: " + \
                ",".join(map(str, self._mostNegative.tags())) + \
                " (" + str(self._mostNegative.count()) + ")"

        print(positiveText + negativeText)
