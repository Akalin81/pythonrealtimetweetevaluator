
import sys
import os
import queue
import json
from scripts.twitterSteamer import Streamer
from scripts.preProcessor import PreProcessor
from scripts.processor import Processor
from scripts.counter import Counter
from scripts.logger import Logger
from scripts.listener import Listener

with open('config.json', 'r') as f:
    config = json.load(f)

# requires "pip install twython && python3" before running
if __name__ == '__main__':
    try:

        # create our preprocessor and processor queues to handle incoming data
        preProcessorQueue = queue.Queue(maxsize=0)
        processorQueue = queue.Queue(maxsize=0)

        # get data
        ignoreList = set(line.strip()
                         for line in open('data/ignoreList.txt'))
        positiveList = set(line.strip()
                           for line in open('data/positive-words.txt'))
        negativeList = set(line.strip()
                           for line in open('data/negative-words.txt'))

        # keep track of running total for our hashtags
        mostPositive = Counter(positiveList)
        mostNegative = Counter(negativeList)

        logger = Logger(mostPositive, mostNegative)

        # start off our twitter streamer using twython
        stream = Streamer(config['CONSUMER_KEY'], config['CONSUMER_SECRET'],
                          config['TOKEN'], config['TOKEN_SECRET'], preProcessorQueue)

        # initialize processors
        preProcessor = PreProcessor(
            preProcessorQueue, processorQueue, ignoreList, config)

        processor = Processor(
            processorQueue, mostPositive, mostNegative, logger)

        # add new twitter listener and start
        listener = Listener(config, preProcessor, processor, stream)
        listener.start()

        processorQueue.join()
        preProcessorQueue.join()
    except KeyboardInterrupt:
        print('Interrupted')
    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)
