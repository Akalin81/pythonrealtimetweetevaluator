This python app uses twython to stream real time tweets using hastags you provide in config file. When it finds a tweet with matching hashtag, it will determine if tweet was positive or negative. It will then keep a running total of positive/negative tags.

to run:

    # add a config.json file to project folder
    {
        "CONSUMER_KEY": "XXXX",
        "CONSUMER_SECRET": "XXXX",
        "TOKEN": "XXXX",
        "TOKEN_SECRET": "XXXX",
        "HASHTAGS": ["KEY1", "KEY2", "KEY3"],
        "PRE_PROCESSOR_THREAD_COUNT": 10,
        "PROCESSOR_THREAD_COUNT": 10
    }


    # needs python 3 and you might also need to set "alias phyton = phyton3" in your ~/.bash_profile
    pip install twython

    # get your twitter api tokens from https://developer.twitter.com
    # update config.json with your tokens
    # update config.json with the hashtags you want to look for

    python start.py
